// This is the form module!
(function (window) {

     // Array to store the people data as objects

   var people = [];
    
   var userAges = [];
    
    
   var userAvatar = function(){
       
          
        var mydIV = document.getElementById('avatarDiv').style.display = 'block';
        var email = document.getElementById('email').value;
        var base = "http://www.gravatar.com/avatar/";
        var hash = MD5(email);

        var fullUrl = base+hash +"jpg";

        document.getElementById("avatar").src=fullUrl;

        document.getElementById("avatar").style.display="block";
   } 

    // function that processes my basic form submission & validation
   var process = function () {
       'use strict';

       // Get form references:
       var userName = document.getElementById('userName').value;
       var fullName = document.getElementById('name').value;
       var password = document.getElementById('pwd').value;
       var email = document.getElementById('email').value;
       var age = document.getElementById('age').value;
       
       //Using the gravatar api to get the users avatar
       var mydIV = document.getElementById('avatarDiv').style.display = 'block';
       var email = document.getElementById('email').value;
       var base = "http://www.gravatar.com/avatar/";
       var hash = MD5(email);

       var fullUrl = base+hash +"jpg";
       
       // Reference to where the output goes:
       var inoutput = document.getElementById('inOutput');

       // a boolean used to to see if a form field is validated or not
       bool = true;
       var error = "";

       // Create a new object:
       var user = {
           uName: userName,
           fName: fullName,
           pass: password,
           uEmail: email,
           uAge: age,
           uPhoto: fullUrl,
           userID: Math.floor((Math.random() * 1000))
       };

       // Form Validation
       if (userName.length !== 0) {

           for (var i = 0; i < people.length; i++) {
               if (userName === people[i].uName) {
                   error += "User name already taken. \n";
                   bool = false;
               }
           }

       }

       if (userName.length === 0) {

           error += "User name field is empty. \n";
           bool = false;

       }

       if (fullName.length === 0) {

           error += "Full name field is empty. \n";
           bool = false;
       }

       if (email.length === 0) {
           error += "Email field is empty. \n";
           bool = false;
       }

       if (email.length > 0) {
           var email = document.getElementById('email').value;
           var atpos = email.indexOf("@");
           var dotpos = email.lastIndexOf(".");
           if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
               error += "Email format not correct. \n";
               return false;
           }
       }

       if (password.length < 8 || password.length > 18) {

           error += "Please enter a password that is between 6 and 18 characters.\n";
           var bool = false;
       }

       if (age.length === 0) {
           error += "Age field is empty. \n";
           bool = false;
       }

       if (!bool) {
           alert(error);
       }

       if (bool) {
           people.push(user);
           userAges.push(user.uAge);
           console.log(people);
           console.log(userAges);
           // calling the updateView function to change the view on the screen
           //updateView(people);

           filter.updateView(people);

           document.getElementById('userName').value = "";
           document.getElementById('name').value = "";
           document.getElementById('pwd').value = "";
           document.getElementById('email').value = "";
           document.getElementById('age').value = "";
           document.getElementById('avatarDiv').style.display = "none";

           // Return false:
           return false;

       }


   }

   var updateView = function (data) {

       var inoutput = document.getElementById('inOutput');

       var message = '';

       for (var i = 0; i < data.length; i++) {
           message += '<div class="view"><div id="outputAvatar"><img src="'+data[i].uPhoto+'" id="avatar"></div>'
           message += '<p class="contactOutput">' + "User Name: " + data[i].uName + '</p>';
           message += '<p class="contactOutput">' + "Full Name: " + data[i].fName + '</p>';
           message += '<p class="contactOutput">' + "Password: " + data[i].pass + '</p>';
           message += '<p class="contactOutput">' + "Email: " + data[i].uEmail + '</p>';
           message += '<p class="contactOutput">' + "Age: " + data[i].uAge + '</p><br></div>';
       }

       inoutput.innerHTML = message;

   }



   var sortName = function () {
       people.sort(function (a, b) {
           if (a.uName < b.uName) return -1;
           if (a.uName > b.uName) return 1;
           return 0;
       })
       updateView(people);
   }

   var checkUsername = function () {

       var userName = document.getElementById('userName').value;
       for (var i = 0; i < people.length; i++) {
           if (userName === people[i].uName) {
               alert("User Name already taken.");
           }
       }
   }

   var checkEmail = function () {
       
       userAvatar();
       var email = document.getElementById('email').value;
       var atpos = email.indexOf("@");
       var dotpos = email.lastIndexOf(".");
       if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
           alert("Not a valid e-mail address");
           return false;
       }
   }

   var sortAge = function () {
       people.sort(function (a, b) {

           return a.uAge - b.uAge;

       })

       filter.updateView(people);
   }


   var filterAge = function (value) {
       if (value == "biggerThen") {
           ageValue = parseInt(document.getElementById('agelimdown').value);

           function isBiggerThen(element) {
               return element.uAge >= ageValue;
           }

           var filterView = people.filter(isBiggerThen);
           filter.updateView(filterView);

       } else if (value == "smallerThen") {

           ageValue = parseInt(document.getElementById('agelimdown').value);

           function isSmallerThen(element) {
               return element.uAge <= ageValue;
           }
           var filterView = people.filter(isSmallerThen);
           filter.updateView(filterView)
       }
   }
   
   // calulate the average age of the all the users
   var averageAge = function(){
       
        
        var AvgAge = userAges.reduce(calculateAvg, 0); 
        alert(AvgAge);
            
   }
   
    function calculateAvg(a, b) {
        
        var e = a + Math.round(b)/userAges.length;
        return e;
    }






   window.filter = window.filter || {};
   window.filter.sortName = sortName;
   window.filter.updateView = updateView;
   window.filter.process = process;
   window.filter.checkUsername = checkUsername;
   window.filter.checkEmail = checkEmail;
   window.filter.sortAge = sortAge;
   window.filter.filterAge = filterAge;
   window.filter.averageAge = averageAge;

   })(window);