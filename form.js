// calling the function from my form module to process the form
function process()
{
    filter.process();
}


// calling the function from my form module to check if the username already exists
function checkUsername()
{
    filter.checkUsername();
}


// calling the function from my form module to check if the email is in the correct format
function checkEmail()
{
    filter.checkEmail();
}


// calling the function from my form module to sort the name in alphabatical order
function sortName(){
    filter.sortName();
}


// calling the function from my form module to sort the age
function sortAge()
{
    filter.sortAge();
}


// calling the function from my form module to filter the age based on user input >= or <=
function filterAge(value)
{
    filter.filterAge(value);
}

// calling the function from my form module to calculate the average age.
function avg()
{
    filter.averageAge();
}

